class InkyBubbles extends InkyXYChart {
    constructor (containerID,inject = null) {
        super (containerID,inject) 
        this.series = [] ;
        this.features = ["XYAxes","CirclesScale","toolTipText"]
        this.seriesArgs = ["scale","tooltiptext"]
    } 
    setXAxis (category = null , nameText = null ,horizontal = null) {
        this.valueAxisX = this.chart.xAxes.push(new am4charts.ValueAxis());
        this.valueAxisX.renderer.ticks.template.disabled = true;
        this.valueAxisX.renderer.axisFills.template.disabled = true; 
    } 

    setYAxis (value = null ,args = null  ) {
        this.valueAxisY = this.chart.yAxes.push(new am4charts.ValueAxis());
        this.valueAxisY.renderer.ticks.template.disabled = true;
        this.valueAxisY.renderer.axisFills.template.disabled = true;
    } 

    addSerie(category ,Xaxis , Yaxis, args) { 
    args = { 
            outline : null , 
             scale  : 1,
             bullet : { strokeWidth : 2 , opacity : 0.8 , tooltipText : "" ,  ...args.bullet },
             ...args
            }
    
    var chart = this.chart ;
    var valueAxisX = this.valueAxisX ; 
    var valueAxisY = this.valueAxisY ; 

    var series = this.chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueX = Xaxis;
    series.dataFields.valueY = Yaxis;
    series.dataFields.value = category ;
    series.strokeOpacity = 0;
    series.sequencedInterpolation = true;
    series.tooltip.pointerOrientation = "vertical";  

    var bullet = this.createBullet(series,args.bullet)
 
    series.heatRules.push({ target: bullet, min: args.scale * 10 , max:  args.scale * 60, property: "radius" });

    if (args.outline)
    {   
        args.outline = {color : "#ff0000",...args.outline}
        this.enableoutline(bullet,this.createOutline(args.outline));
    }
    
 
    this.chart.cursor = new am4charts.XYCursor();
    this.chart.cursor.behavior = "zoomXY"; 
    this.chart.scrollbarX = new am4core.Scrollbar();
    this.chart.scrollbarY = new am4core.Scrollbar(); 
    } 

    addSerieEditor(category ,Xaxis , Yaxis, args) { 
        // Create Series Via Editor
        let editorArgs = { 
            outline : { color : "#000000"}, 
            scale  :  args.scale,
            bullet : { tooltipText : args.tooltiptext } 
            }; 

        args = { 
                 outline : null , 
                 scale  : 1,
                 bullet : {strokeWidth : 2 , opacity : 0.8 , tooltipText : "" ,  ...args.bullet},
                 ...editorArgs
                }
        
        var chart = this.chart ;
        var valueAxisX = this.valueAxisX ; 
        var valueAxisY = this.valueAxisY ; 
    
        var series = this.chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueX = Xaxis;
        series.dataFields.valueY = Yaxis;
        series.dataFields.value = category ;
        series.strokeOpacity = 0;
        series.sequencedInterpolation = true;
        series.tooltip.pointerOrientation = "vertical";  
    
        var bullet = this.createBullet(series,args.bullet)
     
        series.heatRules.push({ target: bullet, min: args.scale * 10 , max:  args.scale * 60, property: "radius" });
    
        if (args.outline)
        {   
            args.outline = {color : "#ff0000",...args.outline}
            this.enableoutline(bullet,this.createOutline(args.outline));
        }
        
        this.series.push(series)
     
        this.chart.cursor = new am4charts.XYCursor();
        this.chart.cursor.behavior = "zoomXY"; 
        this.chart.scrollbarX = new am4core.Scrollbar();
        this.chart.scrollbarY = new am4core.Scrollbar(); 
    } 

    createSeries (category ,Xaxis , Yaxis, args)  {

        args = { 
            outline : null , 
             scale  : 1,
             bullet : { strokeWidth : 2 , opacity : 0.8 , tooltipText : "" ,  ...args.bullet },
             ...args
            }
    
    var chart = this.chart ;
    var valueAxisX = this.valueAxisX ; 
    var valueAxisY = this.valueAxisY ; 

    var series = this.chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueX = Xaxis;
    series.dataFields.valueY = Yaxis;
    series.dataFields.value = category ;
    series.strokeOpacity = 0;
    series.sequencedInterpolation = true;
    series.tooltip.pointerOrientation = "vertical";  

    var bullet = this.createBullet(series,args.bullet)
 
    series.heatRules.push({ target: bullet, min: args.scale * 10 , max:  args.scale * 60, property: "radius" });

    if (args.outline)
    {   
        args.outline = {color : "#ff0000",...args.outline}
        this.enableoutline(bullet,this.createOutline(args.outline));
    }
    
    return series;

    }

    createBullet (series,args) {
        var bullet = series.bullets.push(new am4core.Circle());
            bullet.fill = am4core.color("#ff0000");
            bullet.stroke = am4core.color("#ffffff");
            bullet.propertyFields.fill = "color"; 
            bullet.strokeOpacity = 0;
            bullet.strokeWidth = args.strokeWidth;
            bullet.fillOpacity = args.opacity; 
            bullet.hiddenState.properties.opacity = 0;
            bullet.tooltipText = args.tooltipText; 
            return bullet;
    }

    loadData() {
        this.chart.data = [
            {
                "title": "Afghanistan",
                "id": "AF",
                "color": "#eea638",
                "continent": "asia",
                "x": 10349.69694102398,
                "y": 60.524,
                "value": 33397058
            },
            {
                "title": "Bosnia and Herzegovina",
                "id": "BA",
                "color": "#d8854f",
                "continent": "europe",
                "x": 7664.15281166303,
                "y": 76.211,
                "value": 3744235
            },
            {
                "title": "Bosnia and Herzegovina",
                "id": "BA",
                "color": "#d8854f",
                "continent": "europe",
                "x": 40664.15281166303,
                "y": 76.211,
                "value": 3744235
            },
            {
                "title": "Brazil",
                "id": "BR",
                "color": "#86a965",
                "continent": "south_america",
                "x": 10383.5405937283,
                "y": 73.667,
                "value": 198360943
            },
            {
                "title": "Brazil",
                "id": "BR",
                "color": "#86a965",
                "continent": "south_america",
                "x": 19383.5405937283,
                "y": 73.667,
                "value": 190360943
            },
            {
                "title": "Brazil",
                "id": "BR",
                "color": "#86a965",
                "continent": "south_america",
                "x": 12383.5405937283,
                "y": 80.667,
                "value": 10360943
            },
            {
                "title": "Brunei",
                "id": "BN",
                "color": "#eea638",
                "continent": "asia",
                "x": 45658.2532642054,
                "y": 78.35,
                "value": 412892
            },
            {
                "title": "Dominican Rep.",
                "id": "DO",
                "color": "#a7a737",
                "continent": "north_america",
                "x": 6978.89657264408,
                "y": 53.181,
                "value":  183339
            },
            {
                "title": "Dominican Rep.",
                "id": "DO",
                "color": "#a7a737",
                "continent": "north_america",
                "x": 6978.89657264408,
                "y": 73.181,
                "value": 10183339
            },
            {
                "title": "Dominican Rep.",
                "id": "DO",
                "color": "#a7a737",
                "continent": "north_america",
                "x": 40978.89657264408,
                "y": 60.181,
                "value": 22183339
            },
            {
                "title": "India",
                "id": "IN",
                "color": "#eea638",
                "continent": "asia",
                "x": 32229.14788745778,
                "y": 66.168,
                "value": 1258350971
            },
            {
                "title": "Indonesia",
                "id": "ID",
                "color": "#eea638",
                "continent": "asia",
                "x": 4379.69981934714,
                "y": 70.624,
                "value": 244769110
            } 
            
        ];
        
    }

    addEntry (Entry){
        var Data = this.chart.data ;  
            Data.push(Entry);  
            this.chart.data = Data;  
    }

    enableoutline (bullet,outline) {
        var chart = this.chart ;
        var valueAxisX = this.valueAxisX ; 
        var valueAxisY = this.valueAxisY ; 

        bullet.events.on("over", function(event) {
            var target = event.target;
            chart.cursor.triggerMove({ x: target.pixelX, y: target.pixelY }, "hard");
            chart.cursor.lineX.y = target.pixelY;
            chart.cursor.lineY.x = target.pixelX - chart.plotContainer.pixelWidth;
            valueAxisX.tooltip.disabled = false;
            valueAxisY.tooltip.disabled = false;
        
            outline.radius = target.pixelRadius + 2;
            outline.x = target.pixelX;
            outline.y = target.pixelY;
            outline.show();
        })
      
        bullet.events.on("out", function(event) {
            chart.cursor.triggerMove(event.pointer.point, "none");
            chart.cursor.lineX.y = 0;
            chart.cursor.lineY.x = 0;
            valueAxisX.tooltip.disabled = true;
            valueAxisY.tooltip.disabled = true;
            outline.hide();
        })

    bullet.adapter.add("tooltipY", function (tooltipY, target) {
        return -target.radius;
    })
    var hoverState = bullet.states.create("hover");
    hoverState.properties.fillOpacity = 1;
    hoverState.properties.strokeOpacity = 1;

    } 

    createOutline(args)
    { 
        var outline = this.chart.plotContainer.createChild(am4core.Circle);
        outline.fillOpacity = 0;
        outline.strokeOpacity = 0.8;
        outline.stroke = am4core.color(args.color);
        outline.strokeWidth = 2;
        outline.hide(0); 
        var blurFilter = new am4core.BlurFilter();
        outline.filters.push(blurFilter);  
        return outline;
    } 

    getFeatures () {
        return this.features;
    }

    getSeriesArgs () {
        return this.seriesArgs;
    }

    setAxis (x,y) {
        this.series[0].dataFields.valueX = x
        this.series[0].dataFields.valueY = y
        this.chart.validateData();
    }

    updateSerie (category ,Xaxis , Yaxis, args) { 
        this.chart.series.removeIndex(0); 
        this.addSerieEditor(category ,Xaxis , Yaxis, args) 
    }


    /** Features Methods */ 

    setXmax(Max){ 
        console.log('XMAX ',Max);
        this.valueAxisX.max = parseInt(Max)
        this.valueAxisX.strictMinMax = true
        this.chart.validateData();
    }

    setYmax(Max){ 
        console.log('YMAX ',Max);
        this.valueAxisY.max = parseInt(Max)
        this.valueAxisX.strictMinMax = true
        this.chart.validateData();
    }
    setXmin (Min){ 
        console.log('XMIN ',Min);
        this.valueAxisX.strictMinMax = true
        this.valueAxisX.min = parseInt(Min)
        this.chart.validateData(); 
    }
    setYmin(Min){ 
        console.log('YMIN ',Min);
        this.valueAxisY.min = parseInt(Min)
        this.valueAxisY.strictMinMax = true
        this.chart.validateData(); 
    }

    circleScale(X) {
        console.log('ScaleFlagged'); 
        
    }

    toolTipText() {
     console.log('TooltipText')
      
    }

}