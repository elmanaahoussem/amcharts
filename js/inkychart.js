class InkyChart {

    constructor (containerID,typechart,inject = null) {
       
        if (inject) { 
            // To Inject Charts Specify chartTitle And Order
           let chartID = containerID + '_' + inject.chartTitle ;
           this.checkOrCreateHeaderTabs(containerID);
           this.addChartTabToHeader(containerID,chartID,inject);
            $("#"+containerID).append('<div id="'+chartID+'" data-tab='+ inject.order +'></div>');
           containerID = chartID ;
        }

        switch (typechart)
        {
            case "Map" :     this.chart = am4core.create(containerID, am4maps.MapChart);   break; 
            case "XYChart" : this.chart = am4core.create(containerID, am4charts.XYChart);  break;
            case "PieChart": this.chart = am4core.create(containerID, am4charts.PieChart); break;
            case "PieChart3D": this.chart = am4core.create(containerID, am4charts.PieChart3D); break;
        }

        this.injection = inject 
        this.typechart = typechart ;

        am4core.useTheme(am4themes_animated);

    }

    checkOrCreateHeaderTabs (containerID) {
        if ($("#"+containerID).find(".tabs-header").length == 0 )
        {
            $("#"+containerID).prepend('<div id="'+containerID+'-tabs" class="tabs-header"></div>');  
        }
    } 

    addChartTabToHeader(containerID,chartID,args) {
        let header = $("#"+containerID +" .tabs-header");
        let tab = $("<a>",{class : "chart-tabs-head-tab " , "data-target" : args.order});
        tab.html(args.chartTitle).appendTo(header);
        tab.click(function() { 
            $("#"+containerID).find("[data-tab]").removeClass("active");
            header.find(".active").removeClass("active"); 
            $(this).addClass("active"); 
            $("#" + containerID + " [data-tab = "+$(this).data("target")+"]").addClass("active");  
        })  
    }
 
    loadData (path) { 
        // Loads Data From URL
        this.chart.dataSource.url = path; 
    }
    createCursor() {
        // Activates Cursor On The Maps
        this.chart.cursor = new am4charts.XYCursor();
    }
 
    createLegend (args) { 
        this.chart.legend = new am4charts.Legend();

        args = { 
            position : "bottom" ,       // Legend Position : Right Left Top Bottom
            templateText : "{name}" ,   // Legend Template Text Like {name} : {value}
            markers : true ,            // Disable Or Enable Chart Markers
            fontsize : 16,              // Legend Text Font Size
            ...args }

        this.chart.legend.position = args.position ;
        this.chart.legend.align = args.position ; 
        this.chart.legend.markers.template.disabled = ! args.markers; 
        this.chart.legend.labels.template.text = args.templateText ;   
        this.chart.legend.fontSize = args.fontsize ; 
    }


}

