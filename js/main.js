

 
/*
let chart18 = new ArticleCharts("chart04","XYChart",{chartTitle : "Bab Alioua",order : 1});
chart18.setXAxis("date",null,270);
chart18.setYAxisDate(null,null); 
chart18.addAxisColumn("value","date","Valeur","#4285f4",null,"vertical"); 
chart18.loadData("data/babalioua2.json");
chart18.createLegend();  
chart18.createCursor();

let chart04 = new ArticleCharts("chart04","XYChart",{chartTitle : "Bab Saadoun",order : 2});
chart04.setXAxis("date",null,270);
chart04.setYAxis(null,null);
chart04.addAxisColumn("value","date","Valeur","#4285f4",null,"vertical"); 
chart04.loadData("data/babsaadoun.json");
chart04.createLegend();  
chart04.createCursor();

let chart05 = new ArticleCharts("chart04","XYChart",{chartTitle : "Ben Arouse",order : 3});
chart05.setXAxis("date",null,270);
chart05.setYAxis(null,null);
chart05.addAxisColumn("value","date","Valeur","#4285f4",null,"vertical"); 
chart05.loadData("data/benarous.json");
chart05.createLegend();  
chart05.createCursor();

let chart06 = new ArticleCharts("chart04","XYChart",{chartTitle : "Bizerte",order : 4});
chart06.setXAxis("date",null,270);
chart06.setYAxis(null,null); 
chart06.addAxisColumn("value","date","Valeur","#4285f4",null,"vertical"); 
chart06.loadData("data/bizerte.json");
chart06.createLegend();  
chart06.createCursor();

let chart07 = new ArticleCharts("chart04","XYChart",{chartTitle : "Gabès",order : 5});
chart07.setXAxis("date",null,270);
chart07.setYAxis(null,null); 
chart07.addAxisColumn("value","date","Valeur","#4285f4",null,"vertical"); 
chart07.loadData("data/gabes.json");
chart07.createLegend();  
chart07.createCursor();

let chart08 = new ArticleCharts("chart04","XYChart",{chartTitle : "Ghazela",order : 6});
chart08.setXAxis("date",null,270);
chart08.setYAxis(null,null); 
chart08.addAxisColumn("value","date","Valeur","#4285f4",null,"vertical"); 
chart08.loadData("data/ghazela.json");
chart08.createLegend();  
chart08.createCursor();

let chart09 = new ArticleCharts("chart04","XYChart",{chartTitle : "Kairouan",order : 7});
chart09.setXAxis("date",null,270);
chart09.setYAxis(null,null); 
chart09.addAxisColumn("value","date","Valeur","#4285f4",null,"vertical"); 
chart09.loadData("data/kairouan.json");
chart09.createLegend();  
chart09.createCursor();

let chart10 = new ArticleCharts("chart04","XYChart",{chartTitle : "Manouba",order : 8});
chart10.setXAxis("date",null,270);
chart10.setYAxis(null,null); 
chart10.addAxisColumn("value","date","Valeur","#4285f4",null,"vertical"); 
chart10.loadData("data/manouba.json");
chart10.createLegend();  
chart10.createCursor();

let chart11 = new ArticleCharts("chart04","XYChart",{chartTitle : "Sfax Groupe Chimique",order : 9});
chart11.setXAxis("date",null,270);
chart11.setYAxis(null,null); 
chart11.addAxisColumn("value","date","Valeur","#4285f4",null,"vertical"); 
chart11.loadData("data/sfaxchimique.json");
chart11.createLegend();  
chart11.createCursor();

let chart12 = new ArticleCharts("chart04","XYChart",{chartTitle : "Sfax Ville",order : 10});
chart12.setXAxis("date",null,270);
chart12.setYAxis(null,null); 
chart12.addAxisColumn("value","date","Valeur","#4285f4",null,"vertical"); 
chart12.loadData("data/sfaxville.json");
chart12.createLegend();  
chart12.createCursor();

let chart13 = new ArticleCharts("chart04","XYChart",{chartTitle : "Sousse",order : 11});
chart13.setXAxis("date",null,270);
chart13.setYAxis(null,null); 
chart13.addAxisColumn("value","date","Valeur","#4285f4",null,"vertical"); 
chart13.loadData("data/sousse.json");
chart13.createLegend();  
chart13.createCursor();
 
  */
 
let chart14 = new ArticleCharts("chart03","map");
chart14.mapGeoData();
chart14.mapProjection();
chart14.mapAddSerie("Section1","", null , null ,
                    {color: "grey"},
                    {tooltip:"{name}",tooltipBc:"#b1b1b1"}
                   );
chart14.mapAddMarker(35.5371,10.4301,"Kairouan");
chart14.mapAddMarker(36.8221612,10.132429799999977,"BG Tunisia");
chart14.mapAddMarker(36.8909,10.1729,"El ghazela");
chart14.mapAddMarker(33.9430,10.0667,"Ghannouch (école primaire, Gabès)");
chart14.mapAddMarker(36.8093,10.0863,"Manouba");
chart14.mapAddMarker(36.7891,10.1738,"Bab Alioua");
chart14.mapAddMarker(36.8496,10.1884,"La Cité des Sciences à Tunis");
chart14.mapAddMarker(36.7411,10.1892,"Parc El Mourouj");
chart14.mapAddMarker(36.8852,10.1599,"Parc Urbain Ennahli");
chart14.mapAddMarker(36.7479,10.2726,"Radès");
chart14.mapAddMarker(33.9165,10.0954,"Groupe Chimique Tunisien Gabes");
chart14.mapAddMarker(34.7121,10.7255,"Sfax groupe chimique");
chart14.mapAddMarker(35.8296,10.6277,"Sousse");
chart14.mapAddMarker(36.8086,10.1589,"Bab Saadoun");
chart14.mapAddMarker(37.2712,9.8726,"Mairie de Bizerte");
chart14.mapAddMarker(34.7337,10.7512,"Sfax Ville");
chart14.mapAddMarker(36.7557,10.2349,"Zone Industrielle Ben Arous");
 
/*
let chart15 = new ArticleCharts("chart05","map");
chart15.mapGeoData(0.9,0.8);
chart15.mapProjection();
chart15.mapAddSerie("null",null, null , null, {color: "grey"}, {tooltip:"{name}",tooltipBc:"#b1b1b1"}, false );
chart15.mapAddMarker(37.162791,9.522608,"Bizerte",{value : 212,radius : 212/5 ,tooltipText : "{title} : {value}"});
chart15.mapAddMarker(36.8086,10.1589,"Bab Saadoun",{value : 183 ,radius : 183/5 ,tooltipText : "{title} : {value}"}); 
chart15.mapAddMarker(34.7337,10.7512,"Sfax Ville",{value : 202 ,radius : 202/5,tooltipText : "{title} : {value}"});
chart15.mapAddMarker(34.7121,10.7255,"Sfax groupe chimique",{value : 163 ,radius : 163/5,tooltipText : "{title} : {value}"});
chart15.mapAddMarker(35.8296,10.6277,"Sousse",{value : 23 ,radius : 23/5,tooltipText : "{title} : {value}"});
chart15.mapAddMarker(36.51102,10.138,"Ben Arous",{value : 66 ,radius : 66/5,tooltipText : "{title} : {value}"});
chart15.mapAddMarker(36.7891,10.1738,"Bab Alioua",{value : 107 ,radius : 107/5,tooltipText : "{title} : {value}"});
chart15.mapAddMarker(36.8093,9.8863,"Manouba",{value : 91 ,radius : 91/5,tooltipText : "{title} : {value}"});
chart15.mapAddMarker(36.8909,10.1729,"Ghazela",{value : 81 ,radius : 81/5,tooltipText : "{title} : {value}"});
chart15.mapAddMarker(35.5371,10.4301,"Kairouan",{value : 98 ,radius : 98/5,tooltipText : "{title} : {value}"});
chart15.mapAddMarker(33.88146,10.0982,"Gabes",{value : 280 ,radius : 280/5,tooltipText : "{title} : {value}"});
 
  
let chart16 = new ArticleCharts("chart06","map");
 
if ($(window).width()<=767) {
  chart16.mapGeoData(3,3,{latitude : 36.78 , longitude : 10.061715247367921});
} else {
  chart16.mapGeoData(2.5,2.5,{latitude : 36.7509283207291 , longitude : 10.090499143450915});
} 

chart16.mapProjection();
chart16.mapAddSerie("null",null,["TN-14","TN-11","TN-12","TN-13", "TN-52"], null, 
{color: "grey"}, {tooltip:"{name}",tooltipBc:"#b1b1b1"}, false ); 
chart16.mapAddMarker(36.8086,10.1589,"Bab Saadoun",{value : 183 ,radius : 183/5 ,tooltipText : "{title} : {value}"}); 
chart16.mapAddMarker(36.65102,10.138,"Ben Arous",{value : 66 ,radius : 66/5,tooltipText : "{title} : {value}"});
chart16.mapAddMarker(36.7891,10.1738,"Bab Alioua",{value : 107 ,radius : 107/5,tooltipText : "{title} : {value}"});
chart16.mapAddMarker(36.8909,10.1729,"Ghazela",{value : 81 ,radius : 81/5,tooltipText : "{title} : {value}"});
chart16.mapAddMarker(36.8093,9.8863,"Manouba",{value : 91 ,radius : 91/5,tooltipText : "{title} : {value}"});
 
$("#mapscontainer").on('mousewheel wheel', function(e) {
  let r = e.originalEvent.deltaY;
  $(window).scrollTop($(window).scrollTop() + r );
 
})

/*

  
  let chart03 = new ArticleCharts("chart04","XYChart",{chartTitle : "Bab Alioua",order : 3});
  chart03.setDateAxis("date");
  chart03.setValueAxis(120,600); 
  chart03.addDateSerie("date","value","Dépassements du seuil optimal tunisien (120μg/m³)","#cccccc"); 
  chart03.addDateSerie("date","value2","Dépassements du seuil limite","#ffc200"); 
  chart03.addDateSerie("date","value3","Valeur","#ffffff",true);   
  chart03.addDataDate("data/babalioua.json");
  chart03.createLegend();  
  chart03.createCursor();
  chart03.chartUnit(" μg/m³");
  chart03.createTrendLine("value","date",[
    { "date": "2007-11-06", "value": 260},
    { "date": "2010-11-06", "value": 260}
  ]);
  
  let chart04 = new ArticleCharts("chart04","XYChart",{chartTitle : "Bab Saadoun",order : 1});
  chart04.setDateAxis("date",null,{changeFormatMonth : "yyyy" ,dateFormatMonth : "MMMM"});
  chart04.setValueAxis(120,600); 
  chart04.addDateSerie("date","value","Dépassements du seuil optimal tunisien (120μg/m³)","#cccccc"); 
  chart04.addDateSerie("date","value2","Dépassements du seuil limite","#ffc200"); 
  chart04.addDateSerie("date","value3","Valeur","#ffffff",true);  
  chart04.addDataDate("data/babsaadoun.json");
  chart04.createLegend();  
  chart04.createCursor();
  chart04.chartUnit(" μg/m³");
  chart04.disablezoom();
  chart04.createTrendLine("value","date",[
    { "date": "2004-01-07", "value": 260},
    { "date": "2010-11-30", "value": 260}
  ]);
 
  let chart05 = new ArticleCharts("chart04","XYChart",{chartTitle : "Ben Arous",order : 11});
  chart05.setDateAxis("date");
  chart05.setValueAxis(120,300); 
  chart05.addDateSerie("date","value","Dépassements du seuil optimal tunisien (120μg/m³)","#cccccc"); 
  chart05.addDateSerie("date","value2","Dépassements du seuil limite","#ffc200"); 
  chart05.addDateSerie("date","value3","Valeur","#ffffff",true);  
  chart05.addDataDate("data/benarous.json");
  chart05.createLegend();  
  chart05.createCursor();
  chart05.chartUnit(" μg/m³");
  chart05.createTrendLine("value","date",[
    { "date": "2004-01-07", "value": 260},
    { "date": "2016-11-20", "value": 260}
  ]);

  let chart06 = new ArticleCharts("chart04","XYChart",{chartTitle : "Bizerte",order : 4}); 
  chart06.setDateAxis("date");
  chart06.setValueAxis(120,800); 
  chart06.addDateSerie("date","value","Dépassements du seuil optimal tunisien (120μg/m³)","#cccccc"); 
  chart06.addDateSerie("date","value2","Dépassements du seuil limite","#ffc200"); 
  chart06.addDateSerie("date","value3","Valeur","#ffffff",true);  
  chart06.addDataDate("data/bizerte.json");
  chart06.createLegend();  
  chart06.createCursor();
  chart06.chartUnit(" μg/m³");
  chart06.createTrendLine("value","date",[
    { "date": "2003-07-26", "value": 260},
    { "date": "2011-12-16", "value": 260}
  ]);


  let chart07 = new ArticleCharts("chart04","XYChart",{chartTitle : "Gabès",order : 5});
  chart07.setDateAxis("date");
  chart07.setValueAxis(120,2000); 
  chart07.addDateSerie("date","value","Dépassements du seuil optimal tunisien (120μg/m³)","#cccccc"); 
  chart07.addDateSerie("date","value2","Dépassements du seuil limite","#ffc200"); 
  chart07.addDateSerie("date","value3","Valeur","#ffffff",true);  
  chart07.addDataDate("data/gabes.json");
  chart07.createLegend();  
  chart07.createCursor(); 
  chart07.chartUnit(" μg/m³");
  chart07.createTrendLine("value","date",[
    { "date": "2007-11-13", "value": 260},
    { "date": "2016-11-15", "value": 260}
  ]);


  let chart08 = new ArticleCharts("chart04","XYChart",{chartTitle : "Ghazela",order : 6});
  chart08.setDateAxis("date");
  chart08.setValueAxis(120,500); 
  chart08.addDateSerie("date","value","Dépassements du seuil optimal tunisien (120μg/m³)","#cccccc"); 
  chart08.addDateSerie("date","value2","Dépassements du seuil limite","#ffc200"); 
  chart08.addDateSerie("date","value3","Valeur","#ffffff",true);  
  chart08.addDataDate("data/ghazela.json");
  chart08.createLegend();  
  chart08.createCursor();
  chart08.chartUnit(" μg/m³");
  chart08.createTrendLine("value","date",[
    { "date": "2007-11-19", "value": 260},
    { "date": "2011-12-11", "value": 260}
  ]);

  let chart09 = new ArticleCharts("chart04","XYChart",{chartTitle : "Kairouan",order : 7});
  chart09.setDateAxis("date");
  chart09.setValueAxis(120,500); 
  chart09.addDateSerie("date","value","Dépassements du seuil optimal tunisien (120μg/m³)","#cccccc"); 
  chart09.addDateSerie("date","value2","Dépassements du seuil limite","#ffc200"); 
  chart09.addDateSerie("date","value3","Valeur","#ffffff",true);   
  chart09.addDataDate("data/kairouan.json");
  chart09.createLegend();  
  chart09.createCursor();
  chart09.chartUnit(" μg/m³");
  chart09.createTrendLine("value","date",[
    { "date": "2008-03-13", "value": 260},
    { "date": "2011-06-06", "value": 260}
  ]);

  let chart10 = new ArticleCharts("chart04","XYChart",{chartTitle : "Manouba",order : 8});
  chart10.setDateAxis("date");
  chart10.setValueAxis(120,500); 
  chart10.addDateSerie("date","value","Dépassements du seuil optimal tunisien (120μg/m³)","#cccccc"); 
  chart10.addDateSerie("date","value2","Dépassements du seuil limite","#ffc200"); 
  chart10.addDateSerie("date","value3","Valeur","#ffffff",true);  
  chart10.addDataDate("data/manouba.json");
  chart10.createLegend();  
  chart10.createCursor();
  chart10.chartUnit(" μg/m³");
  chart10.createTrendLine("value","date",[
    { "date": "2007-11-08", "value": 260},
    { "date": "2010-08-05", "value": 260}
  ]);


  let chart11 = new ArticleCharts("chart04","XYChart",{chartTitle : "Sfax Groupe Chimique",order : 9});
  chart11.setDateAxis("date");
  chart11.setValueAxis(120,500); 
  chart11.addDateSerie("date","value","Dépassements du seuil optimal tunisien (120μg/m³)","#cccccc"); 
  chart11.addDateSerie("date","value2","Dépassements du seuil limite","#ffc200"); 
  chart11.addDateSerie("date","value3","Valeur","#ffffff",true);   
  chart11.addDataDate("data/sfaxchimique.json");
  chart11.createLegend();  
  chart11.createCursor();
  chart11.chartUnit(" μg/m³");
  chart11.createTrendLine("value","date",[
    { "date": "2005-10-17", "value": 260},
    { "date": "2016-11-20", "value": 260}
  ]);

  let chart12 = new ArticleCharts("chart04","XYChart",{chartTitle : "Sfax Ville",order : 2});
  chart12.setDateAxis("date");
  chart12.setValueAxis(120,500); 
  chart12.addDateSerie("date","value","Dépassements du seuil optimal tunisien (120μg/m³)","#cccccc"); 
  chart12.addDateSerie("date","value2","Dépassements du seuil limite","#ffc200"); 
  chart12.addDateSerie("date","value3","Valeur","#ffffff",true);  
  chart12.addDataDate("data/sfaxville.json");
  chart12.createLegend();  
  chart12.createCursor();
  chart12.chartUnit(" μg/m³");
  chart12.createTrendLine("value","date",[
    { "date": "2005-09-15", "value": 260},
    { "date": "2016-09-16", "value": 260}
  ]);

  let chart13 = new ArticleCharts("chart04","XYChart",{chartTitle : "Sousse",order : 10});
  chart13.setDateAxis("date",null,{changeFormatDay : "[bold]MMM yyyy[/]"});
  chart13.setValueAxis(120,500); 
  chart13.addDateSerie("date","value","Dépassements du seuil optimal tunisien (120μg/m³)","#cccccc"); 
  chart13.addDateSerie("date","value2","Dépassements du seuil limite","#ffc200"); 
  chart13.addDateSerie("date","value3","Valeur","#ffffff",true);  
  chart13.addDataDate("data/sousse.json");
  chart13.createLegend();  
  chart13.createCursor();
  chart13.chartUnit(" μg/m³");
  $('*[data-target="1"]').click();
  chart13.createTrendLine("value","date",[
    { "date": "2006-02-23", "value": 260},
    { "date": "2011-11-22", "value": 260}
  ]);
 
 /*
 
  let chart01 ;
  if ($(window).width()<=767) {
    chart01 = new ArticleChartMobile("chart01","XYChart",null,{width:90});
  } else {
    chart01 = new ArticleCharts("chart01","XYChart",null,{width:90});
  } 
chart01.setXAxis("region",null,null,{start:0.2,end:0.8});
chart01.setYAxis("Nombre de jours",1500); 
chart01.addAxisColumn("tauxinf","region","Dépassements du seuil optimal tunisien (120μg/m³)","#cccccc","null",false,false,{columnWidth : 100 });
chart01.addAxisColumn("tauxbet","region","Compris entre la valeur limite de l'OMS et celle de la Tunisie","#ffc200","null",false,false,{columnWidth : 100});
chart01.addAxisColumn("tauxsup","region","Dépassements du seuil limite tunisien","#e85c58","null",false,false,{columnWidth : 100});
chart01.loadData("data/chart01.json");
chart01.createLegend();  
chart01.createCursor(); 
chart01.reverseChart(); 

  
 
let chart02 = new ArticleCharts("chart02","XYChart",null,{width:90});
chart02.setXAxis("year");
chart02.setYAxis("Nombre de dépassements",20);
chart02.setGap(80);
chart02.addAxisLine("gabes","year","Gabes","#e85c58");
chart02.addAxisLine("sfaxc","year","Sfax Groupe Chimique","#bfbfbf");
chart02.addAxisLine("sfaxv","year","Sfax Ville","#ffc200");
chart02.loadData("data/chart02.json");
chart02.createLegend();  
chart02.createCursor(); 
chart02.setMinGridDistance(80);
 
 */