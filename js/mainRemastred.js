/*
XY Chart Trend Exemple
let chart02 = new InkyXYChart("chart01");
chart02.setXAxis("year");
chart02.setYAxis(null,20);
chart02.setGap(30);
chart02.addTrendLine("gabes","year","Gabes","#4285f4");
chart02.addTrendLine("sfaxc","year","Sfax Groupe Chimique","#db4437");
chart02.addTrendLine("sfaxv","year","Sfax Ville","#db4437");
chart02.loadData("data/chart02.json");
chart02.createLegend();  
chart02.createCursor(); 
*/
 
 /*
Pie1Args = { 
    innerRadius : 30 ,
    enableTricks: true ,
    tooltipText :"{country} : {litres} Litres",
    onHover : {opacity :0.5} ,
    border : {width : 3 ,color : "white"},
    animation : true , 
    legendOnChart : true ,
    labelsOnChart : false , 
} 
chart02 = new InkyPie("chart01","PieChart3D");
chart02.CreateSerie("country","litres",Pie1Args); 
chart02.loadData("data/chart04.json");
chart02.createLegend();  
 

/*
 
let MapArgs = {
    templateArgs : {color : "#636363"},
    hoverable : {tooltip : "{name}" , opacity : 0.5},
};
let map = new InkyMaps("chart01");
map.GeoData();
map.Projection();
map.AddSerie("Tunisia",MapArgs);
 
map.addPieOnChart("Region","value",{ 
    latitude : 36.162791,
    longitude : 9.922608 ,
    data : [{
        "category": "Bab Alioua",
        "value": 107}, 
        {
        "category": "Ben Arous",
        "value": 66 },
        {
        "category": "Manouba",
        "value": 91 },
        {
        "category": "Ghazela",
        "value": 81
       }]
});

map.addMapLine ({
    location1Name : "Location1",
    location1lat : 35.301229 ,
    location1long : 8.906627,
    location2Name : "Location2",
    location2lat : 33.762709 ,
    location2long : 9.741800,
}) ;
//map.AddCircleMarker(35.162791,9.922608,"Zone1",{ value : 212,radius : 212/5 ,tooltipText : "{title} : {value}"});
map.createLegend({position : "left"});  

 */

 

let bubbles = new InkyBubbles("chart01");
bubbles.setXAxis();
bubbles.setYAxis(); 


var Editor = new ameditor("editor01","chart01",bubbles);


