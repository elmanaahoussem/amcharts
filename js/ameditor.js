class ameditor {
    
    constructor (containerID ,chartID = null,thechart = null) {
        this.patterns = 1 ;                     // Number of data Patterns Created
        this.dataModels = 0 ;                   // Number of Data Models / Slots Created 
        this.editorContainer = containerID ;    // Container ID of the Editor
        this.chartID = chartID ;                // Chart ID
        this.chart = thechart ;                 // Chart Instance 
        this.chartState = "unbuilt"             // Chart Status > The Chart That the Editor Handels
        this.eventListeners = [] ;              // EventListeners Array
        this.renderChartFeatures();             // Get Chart Features And Render its inputs 
    }
  
    addNewPattern() {
       let render =  `<div class="pattern-name-input" id="pattern-${++this.patterns}">
                       <span>Pattern ${this.patterns} : </span>
                       <input type="text" name="pattern-${this.patterns}"  /></br>
                       <input type="radio" name="xaxis" value="xaxis-pttrn-${this.patterns}" data-pattern="${this.patterns}"> XAxis 
                       <input type="radio" name="yaxis" value="yaxis-pttrn-${this.patterns}" data-pattern="${this.patterns}"> YAxis 
                       <input type="radio" name="value" value="value-pttrn-${this.patterns}" data-pattern="${this.patterns}"> Value 
                       <button class="controls-btn" onclick="Editor.removePattern(this)" >X</button>
                       </div> ` ; 
        $("#editor01 .pattern-inputs").append(render); 
    }

    removePattern(element) {
         this.patterns--;
         $(element).parent().remove()
    }
 
    addDataModel()
    { 
        let renderF = `<div class="data-input mb-5" id="data-${++this.dataModels}">`; 
        for(let i=1 ; i <= this.patterns ; i++) { 
            const field = $('input[name=pattern-'+i+']').val(); 
            let render = null ;
            if (field == "color"){
             render = ` <div class="row data-single-input">
                <span class="col-3"> ${field}: </span></br>
                <input type="color" name="data-${field}" class="col" data-pattern="${field}"/>
                </div>` ; 
            }else {
             render = ` <div class="row data-single-input">
                             <span class="col-2"> ${field}: </span>
                             <input type="text" name="data-${field}" class="col" data-pattern="${field}"/>
                             </div>` ; 
           }
           renderF += render;
        } 
        renderF += `<button class="controls-btn" onclick="Editor.removeDataModel(this)" >DELETE ENTRY X</button></div> `;
        $("#data-editor .data-inputs").append(renderF);   
    }

    removeDataModel(element) {
        this.dataModels--;
        $(element).parent().remove()
    }
  
    applyData() 
    { 
        // Used to Apply The New Settings on the Chart

        let Data = []; 
        // Collects the Names of the Data Patterns
        let Xaxis =  $('input[name=pattern-'+$("input[name='xaxis']:checked").data("pattern")+']').val()
        let YAxis =  $('input[name=pattern-'+$("input[name='yaxis']:checked").data("pattern")+']').val()
        let Value =  $('input[name=pattern-'+$("input[name='value']:checked").data("pattern")+']').val()
        
        // Creates Entries to Push into the Final Data[] * Creating Data *
        $('#data-editor .data-inputs').children().each(function () {
            const newEntry = {} 
            $(this).find("input").each(function() {
                newEntry[$(this).data("pattern")] = $(this).val() ; 
            })  
            Data.push(newEntry)
        })   
        
        let args = this.getSeriesArgs() ;

       if (this.chartState ==="unbuilt")
       { 
        this.chart.addSerieEditor(Value,Xaxis,YAxis,args)
        this.chartState = "built"; 
       } else if (this.chartState ==="built") {  
        this.chart.setAxis(Xaxis,YAxis);
        this.chart.updateSerie(Value,Xaxis,YAxis,args) 
       } 

        this.chart.chart.data = Data ; 

    }
 
    getSeriesArgs () {
        // Different Charts .. Needs Different Charts 
        let args = {}
        let serieArgs = this.chart.getSeriesArgs()
        for ( let o in serieArgs ) {
            args[serieArgs[o]] = $("#"+this.editorContainer).find("[data-feature-name='"+serieArgs[o]+"']").val(); ;
        } 
        return args
    }
 
    setListener (FieldId,callback) {
        // Used To Create Event Listners For The Features of the Chart By "Features.js"
        const eventListner = {
            FieldId : FieldId,
            callback : callback
        } 
        this.eventListeners.push (eventListner)
    }

    startListeners() {
       // Used to Start the Event Listners after Setting them in "eventListers[]"
       let self = this ;
        this.eventListeners.forEach ( function (Event) { 
            $("#" + Event.FieldId).change(function(){ 
                self.chart[Event.callback]($(this).val())   
            });
        })
    }
 
    renderChartFeatures () { 

        // Used to Render Chart Features after getting The features list from the chart 
         let chartFeatures = this.chart.getFeatures(); 
        // Unique Label chrt01 is Important in order for Event Listners to work properly 

        let features = new Features("chart01","chrt01","chart-options",this) ; 
        chartFeatures.forEach (
            function(feature) {
                features[feature]()
        })
        this.startListeners();

    }
    



}


