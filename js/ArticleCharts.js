class ArticleCharts {

    constructor (containerID,typechart,inject = null) {
 
        this.inject = inject
        if (inject){ 
           let chartID = containerID + '_' + inject.chartTitle ;
           this.checkHeaderTabs(containerID);
           this.addChartToHeader(containerID,chartID,inject);
            $("#"+containerID).append('<div id="'+chartID+'" data-tab='+ inject.order +'></div>');
           containerID = chartID ;
        }
        if (typechart=="map")
        {
            this.chart= am4core.create(containerID, am4maps.MapChart); 
        }else{
            this.chart = am4core.create(containerID,typechart);
        } 
        this.typechart = typechart ;
        am4core.useTheme(am4themes_animated);

        
    
    }

    checkHeaderTabs (containerID) {
        if ($("#"+containerID).find(".tabs-header").length == 0 )
        {
            $("#"+containerID).prepend('<div id="'+containerID+'-tabs" class="tabs-header row"></div>');  
        }
    }

    addChartToHeader(containerID,chartID,args) {
        let header = $("#"+containerID +" .tabs-header");
        let tab = $("<a>",{class : "chart-tabs-head-tab " ,
                            "data-target" : args.order});
        tab.html(args.chartTitle).appendTo(header);
        tab.click(function() { 
            $("#"+containerID).find("[data-tab]").removeClass("active");
            header.find(".active").removeClass("active"); 
            $(this).addClass("active"); 
            $("#" + containerID + " [data-tab = "+$(this).data("target")+"]").addClass("active"); 
           
        })

        if (args.order == 1) 
        tab.click();
    }

    mapGeoData (maxzoom = null , homezoom = null ,geoZoom = null ) {
        this.chart.geodata = am4geodata_tunisiaHigh;
        
            /** Disable Zoom And Drag       */ 
            this.chart.seriesContainer.draggable = false;
            this.chart.seriesContainer.resizable = false;
            
            if (geoZoom)
            {
              this.chart.homeGeoPoint = {
                    latitude: geoZoom.latitude,
                    longitude: geoZoom.longitude
                  };
            } 
            if (maxzoom){
                this.chart.maxZoomLevel = maxzoom;
                this.chart.minZoomLevel = maxzoom;
            }
          
            if (homezoom)
            this.chart.homeZoomLevel  = homezoom;
     

    }

    mapProjection () {
        this.chart.projection = new am4maps.projections.Miller(); 
    }
 
    mapAddSerie (name = null,containerName = null , include = null  , exclude = null , templateArgs = null , hoverable = false , showinLegend = true) {

        let themap = this.chart;
        let polygonSeries = this.chart.series.push(new am4maps.MapPolygonSeries());
        let polygonTemplate = polygonSeries.mapPolygons.template;
 
        polygonSeries.hiddenInLegend = ! showinLegend;
 
        polygonSeries.name = name
        polygonSeries.useGeodata = true;

        if (exclude)
        {
            polygonSeries.exclude = exclude ;
        }
         
        if (include)
        {
            polygonSeries.include = include ;
        }
        
        

        if (templateArgs)
        { 
            if(templateArgs.color)
            {
                polygonTemplate.fill = templateArgs.color ;  
                polygonSeries.fill = templateArgs.color ;
                
            }
        } 
        if(hoverable)
        {    
            let hs = polygonTemplate.states.create("hover");
            hs.properties.fill = templateArgs.color; 
            if(hoverable.tooltip)
            polygonTemplate.tooltipText = hoverable.tooltip ; 
         

            polygonSeries.tooltip.getFillFromObject = false;
            polygonSeries.tooltip.background.fill = am4core.color(hoverable.tooltipBc);
            
        }   
 
        polygonTemplate.propertyFields.fill = "fill";
 
        var label = this.chart.chartContainer.createChild(am4core.Label);
        label.text = containerName;
    }

    mapAddMarker (lat,long,name,args = null ) {
        let imageSeries = this.chart.series.push(new am4maps.MapImageSeries());
        imageSeries.name = name
        imageSeries.fill = am4core.color("#fec200");
    
       
        let imageSeriesTemplate = imageSeries.mapImages.template;
        let circle = imageSeriesTemplate.createChild(am4core.Circle);

        args = { radius : 16 , strokeWidth : 2, tooltipText : "{title}", value: null , createMarker:false , ...args }
        imageSeries.legendSettings.valueText  = args.value;



        circle.radius = args.radius ;
        circle.fill = am4core.color("#fec200");
        circle.stroke = am4core.color("#FFFFFF");
        circle.strokeWidth = args.strokeWidth ;
        circle.nonScaling = true;
        circle.tooltipText = args.tooltipText;
        

        imageSeriesTemplate.propertyFields.latitude = "latitude";
        imageSeriesTemplate.propertyFields.longitude = "longitude";
        imageSeries.data = [{ 
            "latitude": lat,
            "longitude": long,
            "title": name,
            "value": args.value 
          },  ];
    }

    CreatePieSerie (category,value, name = null) { 
        let serie = this.chart.series.push(new am4charts.PieSeries());
        serie.dataFields.category = category;
        serie.dataFields.value = value; 
    }

    setXAxis (category , nameText = null ,horizontal = null, args = null ,fontSize=16) {
        this.categoryAxis = this.chart.xAxes.push(new am4charts.CategoryAxis()); 
        this.categoryAxis.dataFields.category = category ;
        this.categoryAxis.title.text = nameText ;
        this.categoryAxis.renderer.grid.template.location = 0;
       
        this.categoryAxis.renderer.grid.template.hidden = true;
        this.categoryAxis.renderer.minGridDistance = 20;
        this.categoryAxis.fontSize = fontSize ;
    
        if (args)
        {
            this.categoryAxis.renderer.cellStartLocation = args.start;
            this.categoryAxis.renderer.cellEndLocation = args.end;
        }
  
        if (horizontal) {
            this.categoryAxis.renderer.labels.template.horizontalCenter = "right";
            this.categoryAxis.renderer.labels.template.verticalCenter = "middle";
            this.categoryAxis.renderer.labels.template.rotation = horizontal;
        }
       
    }

    setMinGridDistance (x) {
        this.categoryAxis.renderer.minGridDistance = x;
    }

    setYAxis (value = null ,max = null ) {
        this.valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis()); 
        this.valueAxis.max = max
        this.valueAxis.title.text = value;   
        this.valueAxis.fontSize = 16 ; 
        this.valueAxis.calculateTotals = true; 
        //this.valueAxis.renderer.grid.template.disabled = true; 
    }
  
    setXAxisFontSize (fs) {
        this.categoryAxis.fontSize = fs ; 
    }

    setYAxisFontSize (fs) {
        this.valueAxis.fontSize = fs ; 
    }

    setGap (value) 
    {
        this.valueAxis.renderer.minGridDistance = value;
    } 

    disablezoom () {
        this.chart.maxZoomLevel = 1;
    }

    addAxisColumn (SerieName,CategoryX,displayname,color,unite=null,orientation=null,stacked = true,args = null ) {
        let serie = this.chart.series.push(new am4charts.ColumnSeries());
            serie.dataFields.valueY = SerieName;
            serie.dataFields.categoryX = CategoryX;
            serie.name = displayname ;

            if (unite == null) {
                serie.tooltipText = "{name}: {valueY} ";
            } else {
               
                serie.tooltipText = "{valueY}";
                /*
                serie.tooltip.getFillFromObject = false;
                serie.tooltip.background.fill = am4core.color("#CCCCCC");
              */
            
            }

            this.categoryAxis.fontSize = 12 ; 

            if (stacked)
            serie.stacked = true;

            if (args){ 
                args = {columnWidth : 100 , start:0 , end :1, ...args}
                serie.columns.template.width = am4core.percent(args.columnWidth);
        
            }

            if (orientation!= null){ 
                serie.tooltip.pointerOrientation = "vertical" ;       
            }

            serie.fill = am4core.color(color);
            serie.stroke = am4core.color(color); 
    }

    addAxisLine (SerieName,CategoryX,displayname,color,withbullet = true,unite=null) {
        let serie = this.chart.series.push(new am4charts.LineSeries());
        serie.dataFields.valueY = SerieName;
        serie.dataFields.categoryX = CategoryX;
        serie.tensionX  = 0.8 ;
        serie.name = displayname ; 
        if (unite == null) {
            serie.tooltipText = "{name}: [bold]{valueY} [/]";
        }else {
            serie.tooltipText = "{name}: [bold]{valueY}"+unite+"[/]";
        } 
        serie.strokeWidth = 4
        serie.strokeOpacity = 0.7;
        serie.fill = am4core.color(color);
        serie.stroke = am4core.color(color);

        serie.sequencedInterpolation = true;
        
        if (withbullet) {
            let bullet = serie.bullets.push(new am4charts.CircleBullet());
            bullet.strokeWidth = 2;
            bullet.stroke = am4core.color(color)
        }
     
    }
 
    loadData (path) { 
        this.chart.dataSource.url = path; 
    }

    createLegend (args) {
        this.chart.legend = new am4charts.Legend();
        args = { position : "bottom" , templateText : "{name}" ,markers : true , ...args }
        this.chart.legend.position = args.position ;
        this.chart.legend.align = args.position ;
        this.chart.legend.markers.template.disabled = ! args.markers;
        this.chart.legend.labels.template.text = args.templateText ;
        
        if ($(window).width()<=767) 
        this.chart.legend.fontSize = 12;
    }

    LegendFontSize (fs) {
        this.chart.legend.fontSize = fs;
    }

    createCursor() {
        this.chart.cursor = new am4charts.XYCursor();
        this.chart.cursor.lineY.disabled = true;
        this.chart.cursor.lineY.hidden = true;
        this.valueAxis.cursorTooltipEnabled = false; 
        this.chart.cursor.fontSize = 12;
        if ($(window).width()<=767)  
          this.chart.cursor.behavior = "none";
         
    }

    /** Updates  */
    disablegrid()
    {
  
    }

    chartUnit (Unit) {
        this.valueAxis.renderer.labels.template.adapter.add("text", function(text) {
            return text + Unit;
          });
    }

    invertAxes () {
        
    }

    getZoomlevel()
    {
        return this.chart.zoomLevel;
    }
    getZoomGeoPoint () 
    {
        return this.chart.zoomGeoPoint ;
    }


    
    setDateAxis (value = null ,minGridDistance = null , args  ) {

        this.dateAxis = this.chart.xAxes.push(new am4charts.DateAxis()); 
        this.dateAxis.renderer.grid.template.disabled = true; 
        this.dateAxis.baseInterval = { "timeUnit": "day", "count": 1  }; 

        args = { dateFormatDay   :"MMM d,yyyy",
                 changeFormatDay :"MMM d,yyyy",
                 dateFormatMonth :"MMMM" ,
                 changeFormatMonth :" yyyy " ,
                 dateFormatYear : " yyyy ",
                 changeFormatYear :" yyyy ",
                 ...args}
 
        this.dateAxis.parseDates = true ;
        this.dateAxis.minPeriod = "MM" ;
   
        this.dateAxis.dateFormats.setKey("day", args.dateFormatDay);
        this.dateAxis.periodChangeDateFormats.setKey("day", args.changeFormatDay);
        
        this.dateAxis.dateFormats.setKey("month", args.dateFormatMonth);
        this.dateAxis.periodChangeDateFormats.setKey("month",args.changeFormatMonth);

        this.dateAxis.dateFormats.setKey("year", args.dateFormatYear);  
        this.dateAxis.periodChangeDateFormats.setKey("year", args.changeFormatYear);
        /*
        this.dateAxis.skipEmptyPeriods = true;
        */
        /*
             
        
           */
          this.chart.scrollbarX = new am4core.Scrollbar();
          this.chart.scrollbarY = new am4core.Scrollbar();
        this.dateAxis.dataFields.category = value;
            
        if (minGridDistance)
        this.dateAxis.renderer.minGridDistance = minGridDistance;
 
        

        /* 
        this.dateAxis.dateFormats.setKey("day", "MMM, yyyy"); 
        this.dateAxis.dateFormatter.dateFormat = "yyyy-MM-dd";
        this.dateAxis.markUnitChange = false;
        */
      
    
        
    }
    setValueAxis (min = null , max = null ) {
        this.valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis()); 
        this.valueAxis.calculateTotals = true;  
        if(min)
        { 
            this.valueAxis.min = min;
            this.valueAxis.max = max;
            this.valueAxis.extraMin = 0.1;
            this.valueAxis.strictMinMax = true
        }
        
    }
    addDateSerie (category,value ,name,color , hiddenInLegend = false)
    {
        var series = this.chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = value;
            series.dataFields.dateX = category;
            series.name = name;  
            series.hiddenInLegend = hiddenInLegend;
            if (value == "value3")
            series.tooltipText = "{name}:  {valueY.total}  ";   
            
            series.fill = am4core.color(color);
            series.stroke = am4core.color(color); 

            series.stacked = true;
    }
    addDataDate (path) { 
        let chart = this.chart ;
        var jqxhr = $.getJSON(path, function(data) { 
            data.forEach(function(entry) {
            let month = parseInt(entry.date.substring(0, 2))-1;
            let day = entry.date.substring(3, 5);
            let year = "20" + entry.date.substring(6, 8);   
            entry.date = new Date(year, month, day)
            });   
            chart.data = data ;
          }) 


    }

    reverseChart () {
        //this.categoryAxis.renderer.opposite = true; 
    
    }


     createTrendLine(Yaxis,Xaxis,data) {
        var trend = this.chart.series.push(new am4charts.LineSeries());
        trend.dataFields.valueY = Yaxis;
        trend.dataFields.dateX = Xaxis;
        trend.strokeWidth = 2
        trend.stroke = trend.fill = am4core.color("#ffc200");
        trend.data = data;
        trend.name = "Seuil limite 260 μg/m³";

        var bullet = trend.bullets.push(new am4charts.CircleBullet());
        bullet.tooltipText = "[bold font-size: 17px]Seuil limite : 260 μg/m³[/]";
        bullet.strokeWidth = 2;
        bullet.stroke = am4core.color("#ffc200")
        bullet.circle.fill = trend.stroke;
      
        var hoverState = bullet.states.create("hover");
        hoverState.properties.scale = 1.7;
      
        return trend;
      };



}


class ArticleChartMobile extends ArticleCharts {
    constructor (containerID,typechart,inject = null,containerArgs = null ) {
        super(containerID,typechart,inject = null)
        containerArgs = {width:100 , height : 100 , layout : "vertical",...containerArgs}


        this.container = am4core.create(containerID, am4core.Container);
        this.container.width = am4core.percent(containerArgs.width);
        this.container.height = am4core.percent(containerArgs.height);
        this.container.layout = containerArgs.layout;


        this.inject = inject
        if (inject){ 
           let chartID = containerID + '_' + inject.chartTitle ;
           this.checkHeaderTabs(containerID);
           this.addChartToHeader(containerID,chartID,inject);
            $("#"+containerID).append('<div id="'+chartID+'" data-tab='+ inject.order +'></div>');
           containerID = chartID ;
        }


        if (typechart=="map")
        {
            this.chart= this.container.createChild(am4maps.MapChart); 
        }else{
            this.chart = this.container.createChild(am4charts.XYChart);
        } 
        this.typechart = typechart ;
        am4core.useTheme(am4themes_animated);

        
    
    }
    setXAxis (category , nameText = null ,horizontal = null, args = null ) {
        this.categoryAxis = this.chart.yAxes.push(new am4charts.CategoryAxis());
        this.categoryAxis.dataFields.category = category ;
        this.categoryAxis.title.text = nameText ;
        this.categoryAxis.renderer.grid.template.location = 0;
       
        this.categoryAxis.renderer.grid.template.hidden = true;
        this.categoryAxis.renderer.minGridDistance = 20;
        this.categoryAxis.fontSize = 16 ; 

        this.categoryAxis.renderer.inversed = true ;
        this.categoryAxis.renderer.grid.template.location = 0 ;
        this.categoryAxis.renderer.cellStartLocation = 0.1 ;
        this.categoryAxis.renderer.cellEndLocation = 0.9 ;
    
        if (args)
        {
            this.categoryAxis.renderer.cellStartLocation = args.start;
            this.categoryAxis.renderer.cellEndLocation = args.end;
        }
  
        if (horizontal) {
            this.categoryAxis.renderer.labels.template.horizontalCenter = "right";
            this.categoryAxis.renderer.labels.template.verticalCenter = "middle";
            this.categoryAxis.renderer.labels.template.rotation = horizontal;
        }
       
    }
    
    setYAxis (value = null ,max = null ) {

        this.valueAxis = this.chart.xAxes.push(new am4charts.ValueAxis()); 
        this.valueAxis.max = max
        this.valueAxis.title.text = value;   
        this.valueAxis.fontSize = 16 ; 

        this.valueAxis.renderer.opposite = true;

    }

    addAxisColumn (SerieName,CategoryX,displayname,color,unite=null,orientation=null,stacked = true,args = null ) {
        let serie = this.chart.series.push(new am4charts.ColumnSeries());
            serie.dataFields.valueX = SerieName;
            serie.dataFields.categoryY = CategoryX;
            serie.name = displayname ;

            if (unite == null) {
                serie.tooltipText = "{name}:  {valueX}  ";
            }else {
                serie.tooltipText = " {valueX} ";
            }

            this.categoryAxis.fontSize = 10 ; 

            if (stacked)
            serie.stacked = true;

            if (args){ 
                args = {columnWidth : 100 , start:0 , end :1, ...args}
                serie.columns.template.width = am4core.percent(args.columnWidth);
        
            }

            if (orientation!= null){ 
                serie.tooltip.pointerOrientation = "vertical" ;       
            }

            serie.fill = am4core.color(color);
            serie.stroke = am4core.color(color); 
    }

    createLegend (args) {
        this.chart.legend = new am4charts.Legend();
        args = { position : "top" , templateText : "{name}" ,markers : true , ...args }
        this.chart.legend.position = "absolute" ;
        this.chart.legend.align = args.position ;
        this.chart.legend.markers.template.disabled = ! args.markers;
        this.chart.legend.labels.template.text = args.templateText ; 
    }

    addAxisLine (SerieName,CategoryX,displayname,color,withbullet = true,unite=null) {
        let serie = this.chart.series.push(new am4charts.LineSeries());
        serie.dataFields.valueX = SerieName;
        serie.dataFields.categoryY = CategoryX;
        serie.tensionX  = 0.8 ;
        serie.name = displayname ; 
        if (unite == null) {
            serie.tooltipText = "{name}: {valueX} ";
        }else {
            serie.tooltipText = "{name}: {valueX}"+unite;
        } 
        serie.strokeWidth = 4
        serie.strokeOpacity = 0.7;
        serie.fill = am4core.color(color);
        serie.stroke = am4core.color(color);

        serie.sequencedInterpolation = true;
        
        if (withbullet) {
            let bullet = serie.bullets.push(new am4charts.CircleBullet());
            bullet.strokeWidth = 2;
            bullet.stroke = am4core.color(color)
        }
     
    }

    setDateAxis (value = null ,minGridDistance = null , args  ) {

        this.dateAxis = this.chart.yAxes.push(new am4charts.DateAxis()); 
        this.dateAxis.renderer.grid.template.disabled = true; 
        this.dateAxis.baseInterval = { "timeUnit": "day", "count": 1  }; 

        args = { dateFormatDay   :"MMM d,yyyy",
                 changeFormatDay :"MMM d,yyyy",
                 dateFormatMonth :"MMMM" ,
                 changeFormatMonth :"yyyy" ,
                 dateFormatYear : "yyyy",
                 changeFormatYear :"yyyy",
                 ...args}
 
        this.dateAxis.parseDates = true ;
        this.dateAxis.minPeriod = "MM" ;
   
        this.dateAxis.dateFormats.setKey("day", args.dateFormatDay);
        this.dateAxis.periodChangeDateFormats.setKey("day", args.changeFormatDay);
        
        this.dateAxis.dateFormats.setKey("month", args.dateFormatMonth);
        this.dateAxis.periodChangeDateFormats.setKey("month",args.changeFormatMonth);

        this.dateAxis.dateFormats.setKey("year", args.dateFormatYear);  
        this.dateAxis.periodChangeDateFormats.setKey("year", args.changeFormatYear);
 
          this.chart.scrollbarX = new am4core.Scrollbar();
          this.chart.scrollbarY = new am4core.Scrollbar();
        this.dateAxis.dataFields.category = value;
            
        if (minGridDistance)
        this.dateAxis.renderer.minGridDistance = minGridDistance;
 
    }

    setValueAxis () {
        this.valueAxis = this.chart.xAxes.push(new am4charts.ValueAxis());
        this.valueAxis.min = 120;
        
       
    }

    addDateSerie (category,value ,name)
    {
        var series = this.chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueX = value;
            series.dataFields.dateY = category;
            series.name = name;  
            series.tooltipText = "{name}: {valueX} ";    
    }
    createLegend (args) {
        this.chart.legend = new am4charts.Legend();
        args = { position : "bottom" , templateText : "{name}" ,markers : true , ...args }
        this.chart.legend.position = args.position ;
        this.chart.legend.align = args.position ;
        this.chart.legend.markers.template.disabled = ! args.markers;
        this.chart.legend.labels.template.text = args.templateText ; 
        this.chart.legend.fontSize = 12;
    }
}