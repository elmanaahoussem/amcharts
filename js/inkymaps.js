class InkyMaps extends InkyChart {
    constructor (containerID,inject = null) {
        super (containerID,"Map",inject) 
    } 

    GeoData () {
        // Choose Map To Load
        this.chart.geodata = am4geodata_tunisiaHigh;
    }

    Projection () {
        // map Projection
        this.chart.projection = new am4maps.projections.Miller(); 
    }
 
    AddSerie (name = null, args) {
        
        let polygonSeries = this.chart.series.push(new am4maps.MapPolygonSeries());
        let theMap = this.chart;
        let polygonTemplate = polygonSeries.mapPolygons.template;
        args = { 
            include : null ,        // Regions to Include in the Map ( To Be Displayed ) []
            exclude : null ,        // Regions to Exclude in the Map ( To Be Displayed ) []
            showinLegend : true,    // either The Series Is Shown in Legend or Not
            hoverable : null ,     // Activate Hover Effects
            templateArgs : null ,   // Template Arguments ( Visuals )
            mapName : null,         // Map Name not the Series
            ...args}

        polygonSeries.name = name 
        polygonSeries.useGeodata = true; 
        polygonSeries.hiddenInLegend = ! args.showinLegend;
   
        if (args.exclude)
        polygonSeries.exclude = args.exclude ;

        

        polygonTemplate.events.on("hit", function(ev) {
            theMap.zoomToMapObject(ev.target);
        });
 
        if (args.templateArgs)
        { 
           let templateArgs = { 
                color : null , // Map Regions Color
                ...args.templateArgs }

            if(templateArgs.color)
            {
                polygonTemplate.fill = templateArgs.color ;  
                polygonSeries.fill = templateArgs.color ;
            }
        } 

        if(args.hoverable)
        {    
            let hoverable = {
                tooltip : null ,    // Activates Tooltip By Setting A Template Text 
                opacity : 1 ,       // Color of the Regions OnHover
                ...args.hoverable} 

            let templateStyle = {
                color : null ,  
                 ...args.templateArgs }

            let hs = polygonTemplate.states.create("hover");

            polygonTemplate.fillOpacity = 1 ; 
            hs.properties.fillOpacity = hoverable.opacity; 

            if(hoverable.tooltip) {

                let tooltipstyle = { 
                text: "{name}",     // ToolTip Text Template
                bg : null ,         // Tooltip Bg Color
                ...hoverable.tooltip,   
                } 
                polygonTemplate.tooltipText = tooltipstyle.text ;  
                if (tooltipstyle.bg){
                    polygonSeries.tooltip.getFillFromObject = false;
                    polygonSeries.tooltip.background.fill = am4core.color(tooltipstyle.bg);
                } 
            }
            
        }    

    
        var label = this.chart.chartContainer.createChild(am4core.Label);
        label.text = args.mapName ;

        
        if (args.include){
            polygonSeries.include = args.include ;
            console.log(polygonSeries.include);
        }

        return polygonSeries ;
    }
 
    addPieOnChart (category,value,args) {

        var pieSeries = this.chart.series.push(new am4maps.MapImageSeries());
        var pieTemplate = pieSeries.mapImages.template;
        
        pieTemplate.propertyFields.latitude = "latitude";
        pieTemplate.propertyFields.longitude = "longitude";
        pieSeries.hiddenInLegend = true;
        var pieChartTemplate = pieTemplate.createChild(am4charts.PieChart);

        pieChartTemplate.adapter.add("data", 
        function(data, target) {
            if (target.dataItem) {
                return target.dataItem.dataContext.pieData;
            }
            else {
                return [];
            }
        }); 

        pieChartTemplate.propertyFields.width = "width";
        pieChartTemplate.propertyFields.height = "height";
        pieChartTemplate.horizontalCenter = "middle";
        pieChartTemplate.verticalCenter = "middle"; 
        var pieTitle = pieChartTemplate.titles.create();
        pieTitle.text = "{title}";

        var pieSeriesTemplate = pieChartTemplate.series.push(new am4charts.PieSeries);
        pieSeriesTemplate.dataFields.category = category ;
        pieSeriesTemplate.dataFields.value = value ;
        pieSeriesTemplate.labels.template.disabled = true;
        pieSeriesTemplate.ticks.template.disabled = true;

        args = {
            title : null ,      // Title Of the Pie chart 
            latitude : null ,   // Lat
            longitude : null ,  // long 
            width:100,          // width 
            height:100 ,        // Height
            data : null ,       // data ARRAY of objects
            ...args}

            pieSeries.data = args ; 
 
        pieSeries.data = [{
            "title": args.title,  
            "latitude": args.latitude,
            "longitude": args.longitude,
            "width":  args.width,
            "height": args.height,
            "pieData": args.data
        }]
        
         
    }
 
    AddCircleMarker (lat,long,name,args = null ) {

        let imageSeries = this.chart.series.push(new am4maps.MapImageSeries());
        let imageSeriesTemplate = imageSeries.mapImages.template;
        let circle = imageSeriesTemplate.createChild(am4core.Circle);
        
        args = {  
            radius : 8 ,               // Circle Radius Width
            strokeWidth : 5,           // Border Width
            tooltipText : "{title}",   // ToolTip Text Template 
            value: null ,              // Value of the Circle ( in legend, tooltip , .... ) 
            color : "#fec200",         // Circle Inner Color
            borderColor : "#FFFFFF" ,  // Circle Border Color
            ...args }


        imageSeries.name = name
        imageSeries.fill = am4core.color(args.color); // Color in Legend 
 
        imageSeries.legendSettings.valueText  = args.value;
  
        circle.radius = args.radius ;
        circle.fill = am4core.color(args.color);
        circle.stroke = am4core.color(args.borderColor);
        circle.strokeWidth = args.strokeWidth ;
        circle.nonScaling = true;
        circle.tooltipText = args.tooltipText;  

        imageSeries.data = [{ 
            "latitude": lat,
            "longitude": long,
            "title": name,
            "value": args.value 
          },  ]; 
        imageSeriesTemplate.propertyFields.latitude = "latitude";
        imageSeriesTemplate.propertyFields.longitude = "longitude";
   
    }

    addMapLine (args) {
        var lineSeries = this.chart.series.push(new am4maps.MapArcSeries());
        args = { location1Name : "",
                 location1Color: "#e03e96",
                 location1lat : "",
                 location1long : "",
                 location2Name :"",
                 location2Color : "#e03e96",
                 location2lat : "",
                 location2long :"",...args}

        lineSeries.hiddenInLegend = true;
        lineSeries.mapLines.template.strokeWidth = 4;
        lineSeries.mapLines.template.stroke = am4core.color("#e03e96");
        lineSeries.mapLines.template.line.controlPointDistance = -0.4;
        lineSeries.data = [{
            "multiGeoLine": [
                [     
                    { "latitude": args.location1lat , "longitude": args.location1long },
                    { "latitude": args.location2lat, "longitude": args.location2long }, 
                ]
            ]
        }]; 
        this.AddCircleMarker(args.location1lat,args.location1long,args.location1Name,{color:args.location1Color,strokeWidth:4/3,radius:4*1.5})
        this.AddCircleMarker(args.location2lat,args.location2long,args.location2Name,{color:args.location2Color,strokeWidth:4/3,radius:4*1.5})
    }
}