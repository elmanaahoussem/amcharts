class Features {
    constructor (Chartname,label,container,editor) {
        this.name = Chartname
        this.label = label
        this.container = container 
        this.editor = editor
    }

    XYAxes () {  
        // Setting Max Min for X and Y Axes : XYCharts
        // Supported Charts [ All Charts Based On XYCharts ]
      let render =  `<div class="chart-option">
                     <span class="col-5">${this.name} X Minimum</span>
                     <input class="col" type="number" name="option-x-min-${this.label}" id ="option-x-min-${this.label}" class="chart-option-input" />
                     </div>
                     <div class="chart-option">
                     <span class="col-5">${this.name} Y Minimum</span>
                     <input class="col" type="number" name="option-y-min-${this.label}" id ="option-y-min-${this.label}" class="chart-option-input" />
                     </div>
                     <div class="chart-option">
                     <span class="col-5">${this.name} X Maximum</span>
                     <input class="col" type="number" name="option-x-max-${this.label}" id ="option-x-max-${this.label}" class="chart-option-input" />
                     </div>
                     <div class="chart-option">
                     <span class="col-5">${this.name} Y Maximum</span>
                     <input class="col" type="number" name="option-y-max-${this.label}" id ="option-y-max-${this.label}" class="chart-option-input" />
                     </div>`; 
        $("#"+this.container).append(render);  
        this.editor.setListener(`option-x-min-${this.label}`,"setXmin")
        this.editor.setListener(`option-y-min-${this.label}`,"setYmin")
        this.editor.setListener(`option-x-max-${this.label}`,"setXmax")
        this.editor.setListener(`option-y-max-${this.label}`,"setYmax")
    }

    CirclesScale () {
        // Setting Bubbles Sizes the Scale 
        // Supported Charts [ Bubbles Chart ]
        let render =  `<div class="chart-option">
                        <span class="col-5">${this.name} Circles Scale</span>
                        <input class="col" type="number" name="option-scale-${this.label}" id ="option-scale-${this.label}" data-feature-name="scale" class="chart-option-input" value=1 />
                        </div>`; 
        $("#"+this.container).append(render);  
        this.editor.setListener(`option-scale-${this.label}`,"circleScale")
    }
 
    toolTipText () {
        // Editing Chart ToolTip 
        // Supported Charts [ All Kind of Charts ]
        let render =  `<div class="chart-option">
                        <span class="col-5">${this.name} ToolTip Text</span>
                        <input class="col" type="text" name="option-tooltiptext-${this.label}" id ="option-tooltiptext-${this.label}" data-feature-name="tooltiptext" class="chart-option-input" />
                        </div>`; 
        $("#"+this.container).append(render);  
        this.editor.setListener(`option-tooltiptext-${this.label}`,"toolTipText")
    }


}