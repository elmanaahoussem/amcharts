class InkyXYChart extends InkyChart {

    constructor (containerID,inject = null) {
        super (containerID,"XYChart",inject)
    }

    setXAxis (category , nameText = null ,horizontal = null) {
        this.categoryAxis = this.chart.xAxes.push(new am4charts.CategoryAxis()); 

        this.categoryAxis.dataFields.category = category ; 
        this.categoryAxis.title.text = nameText ;

        // Set Start Position 
        this.categoryAxis.renderer.grid.template.location = 0;
        
        // Set Grid Width
        this.categoryAxis.renderer.minGridDistance = 20;
        
        // Set The Font Size
        this.categoryAxis.fontSize = 16 ; 
        
        // Set Text Orientation 
        if (horizontal) {
            this.categoryAxis.renderer.labels.template.horizontalCenter = "right";
            this.categoryAxis.renderer.labels.template.verticalCenter = "middle";
            this.categoryAxis.renderer.labels.template.rotation = horizontal;
        }
       
    }

    setYAxis (value = null ,args ) {
        this.valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis()); 
        args = { 
            max: null,       // Set Font Size Of Value Axis
            fontsize : 16 , // Set Maximum Value Of the Value Axis
             ...args }
 
        this.valueAxis.title.text = value;    
        this.valueAxis.fontSize = args.fontsize ;  
        this.valueAxis.max = args.max
    }
  
    setGap (value) 
    {
        // Set Gap Between Values in ValueAxis
        this.valueAxis.renderer.minGridDistance = value;
    }

    addTrendLine (SerieName,CategoryX,displayname,color, args) {
        let serie = this.chart.series.push(new am4charts.LineSeries());

        serie.dataFields.valueY = SerieName;
        serie.dataFields.categoryX = CategoryX;
        serie.name = displayname ; 

        args = { 
            withbullet : true ,      // Line With Bullet At Values or Not
            unite : null ,           // Unit accepts String 
            strokeWidth : 2,         // Line Width    
            strokeOpacity : 0.5,     // Line Opacity
            tooltipText : "{name}",  // ToolTip Text
             ...args }
        
        serie.tooltipText = args.tooltipText;
        if (args.unite == null) {
            serie.tooltipText = "{name}: [bold]{valueY} [/]";
        }else {
            serie.tooltipText = "{name}: [bold]{valueY}"+unite+"[/]";
        } 

        if (args.withbullet) {
            let bullet = serie.bullets.push(new am4charts.CircleBullet());
            bullet.strokeWidth = 2;
            bullet.stroke = am4core.color(color)
        }

        serie.strokeWidth = args.strokeWidth ;
        serie.strokeOpacity = args.strokeOpacity ;
        serie.fill = am4core.color(color);
        serie.stroke = am4core.color(color); 
    }





    addAxisColumn (SerieName,CategoryX,displayname,color,args) {
        let serie = this.chart.series.push(new am4charts.ColumnSeries());
            serie.dataFields.valueY = SerieName;
            serie.dataFields.categoryX = CategoryX;
            serie.name = displayname ;

            args = { 
                unite : null ,          // Unit accepts String 
                tooltipText : "{name}", // Tooltip Template Text 
                stacked : false,        // Stacked Charts  
                ...args }

            serie.stacked = args.stacked ; 

            serie.fill = am4core.color(color);
            serie.stroke = am4core.color(color);

            serie.tooltipText = args.tooltipText;
            if (args.unite == null) {
                serie.tooltipText = "{name}: [bold]{valueY} [/]";
            }else {
                serie.tooltipText = "{name}: [bold]{valueY}"+unite+"[/]";
            }
            
    }

}