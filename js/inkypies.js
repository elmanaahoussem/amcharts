class InkyPie extends InkyChart {
    constructor (containerID,Pie3D= "PieChart",inject = null) {
        super (containerID,Pie3D,inject)
        this.Pie3D = Pie3D ;
    }

    CreateSerie (category,value, args) { 
        args = {
            innerRadius : 0 ,       // cut a hole in our Pie chart % Value
            border : null,          // Creates a Border Around the Pie
            enableTricks : true,    // Enables Tricks and labels on the pie 
            legendOnChart : true,   // Enables Legend Over the chart
            tooltipText : "",       // Enables Tool Tip
            onHover : null ,        // Enables On Hover Effects
            animation : null ,      // Enables Animations 
            options3d : null ,      // 3D Options (Only with 3D Versions)
            dataFormat : null,      // The Way Data is Represented
            legendOnChart : false,  // Sets the Legend on Pie
            labelsOnChart : false,  // Stick the Legend to the Pie
            ...args };

        let serie = null ; 

        if (this.Pie3D != "PieChart")
        {
            serie = this.chart.series.push(new am4charts.PieSeries3D());
        } else {
            serie = this.chart.series.push(new am4charts.PieSeries());
        }

        if (args.animation) {
            this.chart.hiddenState.properties.opacity = 0;    
        }

        if (args.dataFormat) {
            let dataFormat = { radiusIsValue : false , ...args.dataFormat}

        if (dataFormat.radiusIsValue)
        serie.dataFields.radiusValue = value;

        }
        
        if (args.options3d) {
            let options3d = { 
                height : 20 ,             // Height Of the Chart 
                heightisValue : false,     // Each height is affected by value
                ...args.options3d }

              if (options3d.heightisValue) {
                serie.dataFields.depthValue = value;
              }  
            this.chart.depth = options3d.height;
        }
     
        serie.slices.template.tooltipText = args.tooltipText;
        this.chart.innerRadius = am4core.percent(args.innerRadius);
        serie.dataFields.category = category;
        serie.dataFields.value = value; 
        
        if (args.onHover) {
            let onHover = {scale : 1.1 , shift : 0 ,opacity : 1 , ...args.onHover } 

            serie.slices.template.fillOpacity = 1;

            var hs = serie.slices.template.states.getKey("hover");
            hs.properties.scale = 1;
            hs.properties.fillOpacity = onHover.opacity;  
            hs.properties.shiftRadius = onHover.shift;
            hs.properties.scale = onHover.scale;
 
        }

        if (args.labelsOnChart) { 
            serie.alignLabels = false;              //Sets Legend on the Pie 
            serie.labels.template.bent = true;      //Sets Legend on the Pie 
            serie.labels.template.radius = 3;       //margin between Texts and graph 
            serie.labels.template.padding(0,0,0,0); //Paddings between Texts and graph  
        } 


        if (!(args.legendOnChart)) {
           
            serie.labels.template.disabled = true;
        }
        if (!(args.enableTricks)){
          
            serie.ticks.template.disabled = true;
        } 

        if (args.border){
            let newBorder = {color : "white" , width: 20 ,opacity : 1 , ...args.border};
            serie.slices.template.stroke = am4core.color(newBorder.color);
            serie.slices.template.strokeWidth = newBorder.width;
            serie.slices.template.strokeOpacity = newBorder.opacity;
        }

        // change the cursor on hover 
        serie.slices.template .cursorOverStyle = [ { "property": "cursor", "value": "pointer" } ];

      
    }

 

}