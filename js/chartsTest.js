const Dataset = function(args){
    this.data = args.data;

    this.axis = {
      y:{
        min:args.yaxis?args.yaxis.min:null,
        max:args.yaxis?args.yaxis.max:null
      }

    }
    this.stacked = args.type.split('-')[0]==='stacked';
    this.palette = args.palette?args.palette:["#388E3C", "#FBC02D","#0288d1", "#F44336", "#8E24AA"];
    this.unit = args.unit;
    this.series = this.buildSeries(this.data, args.series);
  }

Dataset.prototype = {
  buildSeries:function(data, series){
    let palette = this.palette;
    if (!series) series = [];
    this.data.map((rowData,row)=>{
      if (row>0 &&  !series[row-1]) series[row-1] = {color:palette[row-1]};
    })
    return series;
  },

  toAmchartData:function(){
    let json = [];
    this.data[0].map((x, colIndex)=>{
      if (colIndex==0) return;
      let r = {};
      r.label = this.getAbscisse(colIndex);
      this.data.map((row,rowIndex)=>{
        if (rowIndex===0) return;
        let lab = this.getSerieLabel(rowIndex);
        let val = this.getValue(rowIndex, colIndex )
        r[lab] = val;
      });
      json.push(r);
    });
   
    return json;


  },
  getGraphs:function(){
    let graphs = [];
    this.data.map((row, index)=>{
      if (index==0) return;
      let       graphData = {};
      graphData.title = this.getSerieLabel(index);
      graphData.field = graphData.title;
      graphData.color = this.series[index-1].color;
      let gdata = this.series[index-1];
      gdata = {color: '#000', thickness:1, dotColor: '#000', ...gdata}
      gdata.color = '#000';
      graphData.thickness = this.series[index-1].thickness;
      graphData.dotColor = this.series[index-1].dotColor;

      graphs.push(graphData);
    });
    return graphs;
  },

  getAbscisse:function(x){
    return this.data[0][x];
  },

  getSerieLabel:function(y){
    return this.data[y][0];
  },

  getValue:function(serie, x){
    return this.data[serie][x];
  }


}

makePieChart = function(elementId, data, args){
  am4core.useTheme(am4themes_animated);
  var chart = am4core.create(elementId, am4charts.PieChart);
  chart.innerRadius = am4core.percent(50);
  chart.data = data.toAmchartData();
  var colorSet = new am4core.ColorSet();
colorSet.list = data.palette.map(function(color) {
  return new am4core.color(color);
});
  const g  = data.getGraphs()[0]
  let serie = chart.series.push(new am4charts.PieSeries());
  serie.dataFields.value = g.field;
  serie.colors = colorSet;
  serie.dataFields.category = 'label';
  serie.slices.template.stroke = am4core.color("#fff");
  serie.slices.template.strokeWidth = 2;
  serie.slices.template.strokeOpacity = 1;
  serie.hiddenState.properties.opacity = 1;
  serie.hiddenState.properties.endAngle = -90;
  serie.hiddenState.properties.startAngle = -90;
  serie.alignLabels = false;
  serie.labels.template.bent = true;
  serie.labels.template.padding(0,0,0,0);
  serie.labels.template.radius = 5;
}

makeLineChart = function(elementId, data, args){
  am4core.useTheme(am4themes_animated);

  var chart = am4core.create(elementId, am4charts.XYChart);
  chart.paddingRight = 20;
  chart.data = data.toAmchartData();

  var xAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  xAxis.dataFields.category = "label";
  xAxis.startLocation = 0.5;
  xAxis.endLocation = 0.5;
  xAxis.renderer.line.strokeOpacity = 1;
  xAxis.renderer.grid.template.disabled = true;


  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.renderer.grid.template.disabled = true;// Pre zoom
  valueAxis.baseValue = 0;
  valueAxis.renderer.line.strokeOpacity = 1;
  if (data.axis.y.min && data.axis.y.max){
    valueAxis.min = parseFloat(data.axis.y.min);
    valueAxis.max = parseFloat(data.axis.y.max);
    valueAxis.strictMinMax = true;
  }

  data.getGraphs().map((g,index)=>{
    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = g.field;
    series.name = g.title;
    series.dataFields.categoryX = 'label';
    series.tensionX = 0.77;
    series.stroke = am4core.color(g.color);
    series.strokeWidth = g.thickness;
    let bullets = series.bullets.push(new am4charts.CircleBullet());
    //bullets.circle.stroke = am4core.color("#FFFFFF");
    bullets.circle.strokeWidth = 3;

    bullets.circle.fill = series.fill =  am4core.color(g.color);
    series.stacked = data.stacked;
    if (series.stacked){
      series.fillOpacity = .5;
    }
    series.tooltipText = "{name} : [bold]{valueY} "+data.unit+'[/bold]';
    series.legendSettings.valueText = "{valueY}";
    series.tooltip.getFillFromObject = false;
    series.tooltip.background.fill = am4core.color(g.color);

  });

  chart.cursor = new am4charts.XYCursor();
  chart.legend = new am4charts.Legend();


}

makeBarChart = function(elementId, data, args){
  am4core.useTheme(am4themes_animated);

  var chart = am4core.create(elementId, am4charts.XYChart);
  chart.paddingRight = 20;
  chart.data = data.toAmchartData();

  var xAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  xAxis.dataFields.category = "label";
  xAxis.renderer.line.strokeOpacity = 1;
  xAxis.renderer.grid.template.disabled = true;



  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.renderer.grid.template.disabled = true;// Pre zoom
  valueAxis.baseValue = 0;
  valueAxis.renderer.line.strokeOpacity = 1;
  if (data.axis.y.min && data.axis.y.max){
    valueAxis.min = parseFloat(data.axis.y.min);
    valueAxis.max = parseFloat(data.axis.y.max);
    valueAxis.strictMinMax = true;
  }

  data.getGraphs().map((g,index)=>{
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = g.field;
    series.name = g.title;
    series.dataFields.categoryX = 'label';
    series.fill = am4core.color(g.color);
    //series.sequencedInterpolation = true;

  // Make it stacked
  series.stacked = data.stacked;
  series.strokeWidth = g.thickness;
  series.columns.width = '100%';

  series.tooltipText = "{name} : [bold]{valueY} "+data.unit+'[/bold]';
  series.legendSettings.valueText = "{valueY}";
  series.tooltip.getFillFromObject = false;
  series.tooltip.background.fill = am4core.color(g.color);

});

  chart.cursor = new am4charts.XYCursor();
  chart.legend = new am4charts.Legend();


}

const convertFeedToData =  feed=>{
  let data  = [];
  feed.map(e=>{
    let row = parseInt(e['gs$cell'].row)-1;
    let col = parseInt(e['gs$cell'].col)-1;
    if (!data[row]) data[row]= [];
    data[row][col] = e['gs$cell']['$t'];
  })

  return data;
}

  
function getSheetData (docId,sheetID)  {
  return new Promise((resolve , reject) => {
            const dataUrl = 'https://spreadsheets.google.com/feeds/cells/'+docId+'/'+sheetID+'/public/values?alt=json';
             $.get(dataUrl, x=>{ resolve({x,sheetID})})
            
        })
 };


$.fn.googleChart = function(args){
  args = {dataSource:'',type:'',sheets:1, ...args};
  if (!args.dataSource){
    $(this).append('<div class="pos-absolute bg-red color-white h6 fs-16px fw-400 fs-400 w-100 bottom-0 direct-0 tex-center p-2">'+'<span class="fa fa-exclamation-circle"></span> Please Select Data Source...</div>')
    return;
  }
  let docId = args.dataSource.split('/spreadsheets/d/')[1].split('/')[0];
  const elementId = $(this).attr('id');
  if (!docId) return;

  for (var i=1 ; i<= args.sheets ; i++) {
    
  const dataUrl = 'https://spreadsheets.google.com/feeds/cells/'+docId+'/'+i+'/public/values?alt=json';
  var chartID  = elementId+'_'+i ;
  $(this).append('<div id="'+chartID+'" data-tab='+i+'></div>');
  getSheetData(docId,i).then( (data)=>{
     var  x = data.x ;
     console.log(x)
     var  i = data.sheetID;
     var chartID  = elementId+'_'+i ;
   
    $(this).append('<div id="'+chartID+'" data-tab='+i+'></div>');
 
    if (!$('body').find('#'+chartID).length) return;
    args.data = convertFeedToData(x.feed.entry);
    switch (args.type){
      case 'bars':
      case 'stacked-bars':
      makeBarChart(chartID, new Dataset(args));
      break;
      case 'pie':
      makePieChart(chartID, new Dataset(args));
      break;
      default:
      makeLineChart(chartID, new Dataset(args));
      break;
    }
  });
}
   if ($("#"+elementId).find("[data-tab]").length > 1) {
       $("#"+elementId).prepend('<div class="chart-tabs-head"></div>');
       var tabs =  $("#"+elementId+" .chart-tabs-head");
       $("#"+elementId).find("[data-tab]").each(function (i,e) {
          var $tab = $(this);
           var tab = $("<a>",{class : "chart-tabs-head-tab " ,
                              "data-target" : "#"+$tab.attr('id')});
            tab.html($tab.attr('id')).appendTo(tabs);
           tab.click(function() {
            $("#"+elementId).find("[data-tab]").removeClass("active");
            tabs.find(".active").removeClass("active");
            $(this).addClass("active");
            $($(this).data("target")).addClass("active");

           })
           if (i == 0) 
            tab.click();
       })
    }else{
        $("#"+elementId).find("[data-tab]").addClass("active");
    } 
  return;
}